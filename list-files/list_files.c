#include <advc/io.h>
#include <advc/mem.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

typedef struct {
    a_str_t filename;
    a_fd_common_info_t common;
} file_info_t;

errno_t
get_file_info(const a_str_t *path, const a_str_t *name, file_info_t *info_ptr)
{
    errno_t e;

    size_t path_size = a_str_get_size(path);
    size_t name_size = a_str_get_size(name);
    size_t filename_size = path_size + name_size;
    bool ends_with_slash = a_str_ends_with(path, A_STR_P("/"));
    if (!ends_with_slash) {
        ++filename_size;
    }

    char *filename_data_copy;
    e = a_mem_alloc(&filename_data_copy, filename_size);
    if (e) {
        return e;
    }
    char *ptr = filename_data_copy;
    ptr = mempcpy(ptr, a_str_get_data(path), path_size);
    if (!ends_with_slash) {
        *ptr = '/';
        ++ptr;
    }
    ptr = mempcpy(ptr, a_str_get_data(name), name_size);

    a_filedes_t fd;
    e = a_file_open_path(A_STR_P_BY_DATA(filename_data_copy, filename_size),
                         &fd);
    if (fd == -1) {
        fprintf(stderr, "%.*s\n", (int) filename_size, filename_data_copy);
        e = errno;
        goto undo_filename;
    }

    e = a_fd_get_common_info(fd, &info_ptr->common);
    if (e) {
        goto undo_open;
    }

    close(fd);

    a_str_take_data(&info_ptr->filename, filename_data_copy, filename_size);
    return A_ERR_OK;

undo_open:
    close(fd);
undo_filename:
    a_mem_free(filename_data_copy);
    return e;
}

void finalize_file_info(file_info_t *info)
{
    a_str_fin(&info->filename);
    a_fd_finalize_common_info(&info->common);
}

void print_file_info(file_info_t *info)
{
    char modified_time[256];
    modified_time[0] = '\0';

    time_t time = info->common.modified_timestamp;
    struct tm *tm;
    struct tm tm_buffer;
    tm = gmtime_r(&time, &tm_buffer);
    if (tm) {
        char *res = asctime_r(tm, modified_time);
        if (res) {
            res[strlen(res) - 1] = '\0';
        }
    }

    printf("%c%c%c%c%c%c%c%c%c%c\t%llu\t%.*s\t%.*s\t%zu\t%s\t%.*s\n",
           info->common.is_dir ? 'd' : '-',
           info->common.owner_read ? 'r' : '-',
           info->common.owner_write ? 'w' : '-',
           info->common.owner_execute ? 'x' : '-',
           info->common.group_read ? 'r' : '-',
           info->common.group_write ? 'w' : '-',
           info->common.group_execute ? 'x' : '-',
           info->common.other_read ? 'r' : '-',
           info->common.other_write ? 'w' : '-',
           info->common.other_execute ? 'x' : '-',
           (unsigned long long) info->common.links_count,
           (int) info->common.owner_user_size,
           info->common.owner_user,
           (int) info->common.owner_group_size,
           info->common.owner_group,
           info->common.size,
           modified_time,
           A_STR_F(&info->filename));
}

errno_t print_file_by_path(const a_str_t *path, const a_str_t *name)
{
    errno_t e;

    file_info_t file_info;
    e = get_file_info(path, name, &file_info);
    if (e) {
        return e;
    }

    print_file_info(&file_info);

    finalize_file_info(&file_info);
    return A_ERR_OK;
}

static errno_t filter_hidden_files_cb(const a_str_t *dir_path,
                                      int dir_fd,
                                      const a_str_t *filename,
                                      void *user_data,
                                      bool *accept_ptr)
{
    const char *filename_ntstr = a_str_get_data(filename);
    if ((filename_ntstr != NULL) && (filename_ntstr[0] == '.')) {
        *accept_ptr = false;
    }
    return A_ERR_OK;
}

errno_t list_files_by_path(const a_str_t *path)
{
    errno_t e;

    bool is_dir = false;
    a_file_is_dir(path, &is_dir);
    if (!is_dir) {
        return print_file_by_path(path, a_str_null);
    }

    a_str_t *files;
    a_len_t files_count;
    e = a_dir_filter(path,
                     filter_hidden_files_cb,
                     NULL,
                     a_str_cmp,
                     &files,
                     &files_count);
    if (e) {
        return e;
    }

    for (a_len_t i = 0; i < files_count; ++i) {
        const a_str_t *filename = &files[i];
        e = print_file_by_path(path, filename);
        if (e) {
            goto undo_files;
        }
    }

    for (a_len_t i = 0; i < files_count; ++i) {
        a_str_fin(&files[i]);
    }
    a_mem_free(files);
    return A_ERR_OK;

undo_files:
    for (a_len_t i = 0; i < files_count; ++i) {
        a_str_fin(&files[i]);
    }
    a_mem_free(files);
    return e;
}

int main(int argc, char *argv[])
{
    if (argc < 1) {
        return EXIT_FAILURE;
    }

    errno_t e;

    for (int i = 1; i < argc; ++i) {
        const a_str_t *filename = A_STR_P(argv[i]);
        e = list_files_by_path(filename);
        if (e) {
            fprintf(stderr, "%s\n", strerror(e));
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
