# Afileutils

## Name
The project name is Advanced file utilities (afileutils). The name may change.

## Description
Project contains list-files utility which is intended to replace the ls utility in Linux systems.

## Compiling
1. Clone sources of the project with submodules into the afileutils-source directory with command:
`git clone --recurse-submodules git@gitlab.com:9fcc/afileutils.git afileutils-source`
1. Create build directory as a subling for the source code directory:
`mkdir build`
1. Change current directory to build:
`cd build`
1. Compile the project and install to a custom local directory:
`cmake -DCMAKE_INSTALL_PREFIX=./local ../afileutils-source && make install`
1. Run the utility (the example display contents of user's home directory):
`./local/bin/list-files ~`

## Contributing
The project is currently in very early development state and closed for contributions. Any ideas for future development are appreciated.

## Authors and acknowledgment
* Dotsenko Andrey

## License
The source code of utilities is licensed by the GPL 2.0 license.

## Project status
This project is in development state and is intended to be used only for testing purposes. This project is a concept-of-proof for the advc library.
